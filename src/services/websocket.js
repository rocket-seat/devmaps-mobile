import socketio from "socket.io-client";
import { uri } from "./api";

const socket = socketio(uri, { autoConnect: false });

function subscribeToNewsDevs(data) {
  socket.on("new-dev", data);
}

function subscribeToDelDevs(data) {
  socket.on("del-dev", data);
}

function connect(latitude, longitude, techs) {
  socket.io.opts.query = {
    isPC: false,
    latitude,
    longitude,
    techs
  };

  socket.connect();
}

function disconnect() {
  if (socket.connected) {
    socket.disconnect();
  }
}

export { connect, disconnect, subscribeToNewsDevs, subscribeToDelDevs };
