import axios from "axios";

// export const uri = "http://10.0.0.7:3333";
export const uri = "https://devmaps-backend.herokuapp.com";

export const api = axios.create({
  baseURL: uri + "/api"
});
