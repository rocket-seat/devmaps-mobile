import React, { useState, useEffect } from "react";
import { StyleSheet, Image, View, Text, TextInput, TouchableOpacity } from "react-native";
import { requestPermissionsAsync, getCurrentPositionAsync } from "expo-location";
import MapView, { Marker, Callout } from "react-native-maps";
import { MaterialIcons } from "@expo/vector-icons";

import { connect, disconnect, subscribeToNewsDevs, subscribeToDelDevs } from "../services/websocket";
import { api } from "../services/api";

export default function Main({ navigation }) {
  const [currentRegion, setCurrentRegion] = useState(null);
  const [devs, setDevs] = useState([]);
  const [techs, setTechs] = useState("");

  useEffect(() => {
    subscribeToNewsDevs(dev => {
      setDevs([...devs, dev]);
    });

    subscribeToDelDevs(dev => {
      setDevs([...devs.filter(d => d._id !== dev._id)]);
    });
  }, []);

  useEffect(() => {
    async function loadInitialPosition() {
      const { granted } = await requestPermissionsAsync();

      if (granted) {
        const { coords } = await getCurrentPositionAsync({
          enableHighAccuracy: true
        });

        const { latitude, longitude } = coords;

        setCurrentRegion({
          latitude,
          longitude,
          latitudeDelta: 0.05,
          longitudeDelta: 0.05
        });
      }
    }

    loadInitialPosition();
  }, []);

  function setupWebSocket() {
    disconnect();

    const { latitude, longitude } = currentRegion;

    connect(latitude, longitude, techs);
  }

  async function loadDevs() {
    const { latitude, longitude } = currentRegion;

    const res = await api.get("/search", {
      params: {
        latitude,
        longitude,
        techs
      }
    });

    setDevs(res.data.devs);
    setupWebSocket();
  }

  function handleRegionChanged(region) {
    setCurrentRegion(region);
    loadDevs();
  }

  if (!currentRegion) {
    return null;
  }

  return (
    <>
      <MapView style={styles.map} initialRegion={currentRegion} onRegionChangeComplete={handleRegionChanged}>
        {devs.map((dev, i) => {
          return (
            <Marker key={i} coordinate={{ longitude: dev.location.coordinates[0], latitude: dev.location.coordinates[1] }}>
              <Image source={{ uri: dev.avatar_url }} style={styles.avatar} />

              <Callout
                onPress={() => {
                  navigation.navigate("Profile", { github_username: dev.github_username });
                }}
              >
                <View style={styles.callout}>
                  <Text style={styles.devName}>{dev.name}</Text>
                  <Text style={styles.devBio}>{dev.bio}</Text>
                  <Text style={styles.devTechs}>{dev.techs.join(", ")}</Text>
                </View>
              </Callout>
            </Marker>
          );
        })}
      </MapView>

      <View style={styles.searchForm}>
        <TextInput value={techs} onChangeText={t => setTechs(t)} style={styles.searchInput} placeholder="Buscar devs por techs..." placeholderTextColor="#999" autoCapitalize="words" autoCorrect={false} />

        <TouchableOpacity style={styles.loadButton} onPress={() => loadDevs()}>
          <MaterialIcons name="my-location" size={20} color="#fff" />
        </TouchableOpacity>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  searchForm: {
    position: "absolute",
    top: 20,
    left: 20,
    right: 20,
    zIndex: 5,
    flexDirection: "row"
  },
  searchInput: {
    flex: 1,
    height: 50,
    backgroundColor: "#fff",
    color: "#333",
    borderRadius: 25,
    paddingHorizontal: 20,
    fontSize: 16,
    shadowColor: "#000",
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 4,
      height: 4
    },
    elevation: 2
  },
  loadButton: {
    width: 50,
    height: 50,
    backgroundColor: "#8e4dff",
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 15
  },

  map: {
    flex: 1
  },
  avatar: {
    width: 54,
    height: 54,
    borderRadius: 4,
    borderWidth: 4,
    borderColor: "#fff"
  },
  callout: {
    width: 260
  },
  devName: {
    fontWeight: "bold",
    fontSize: 16
  },
  devBio: {
    color: "#666",
    marginTop: 5
  },
  devTechs: {
    marginTop: 5
  }
});
